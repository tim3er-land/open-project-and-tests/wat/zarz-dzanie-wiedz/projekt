package pl.wat.zawi.projekt.stage;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pl.wat.zawi.projekt.scene.EdithScene;
import pl.wat.zawi.projekt.scene.MainScene;

import java.util.List;

/**
 * @author: Piotr Witowski
 * @date: 09.01.2022
 * @project projekt
 * Day of week: niedziela
 */
public class MainStage extends Application {

    public static final int WIDTH = 1920;
    public static final int HEIGHT = 1080;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setScene(prepareScene(new MainScene().makeScene(primaryStage)));
//        primaryStage.setScene(prepareScene(new InfoScene().makeScene(primaryStage)));
//        primaryStage.setScene(prepareScene(new PredictScene().makeScene(primaryStage)));
//        primaryStage.setScene(prepareScene(new EdithScene().makeScene(primaryStage)));
        primaryStage.show();

    }

    public Pane addElementToPane(Pane root, List<Node> nodes) {
        root = new Pane();
        for (Node node : nodes) {
            root.getChildren().add(node);
        }
        return root;
    }

    public Scene prepareScene(Pane root) {
        return new Scene(root, WIDTH, HEIGHT);
    }
}
