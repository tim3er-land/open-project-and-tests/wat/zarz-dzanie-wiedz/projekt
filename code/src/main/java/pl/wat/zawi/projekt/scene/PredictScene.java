package pl.wat.zawi.projekt.scene;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import pl.wat.zawi.projekt.OwlService;
import pl.wat.zawi.projekt.stage.MainStage;
import uk.ac.manchester.cs.owl.owlapi.OWLClassImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author: Piotr Witowski
 * @date: 09.01.2022
 * @project projekt
 * Day of week: niedziela
 */
public class PredictScene extends CustomScene {
    private static HashMap<String, List<String>> selectedMap = new HashMap<>();
    private static HashMap<String, List<String>> stringListHashMap = new HashMap<>();
    private static List<String> stanowsika = new ArrayList<>();

    public PredictScene() {
        stringListHashMap = new HashMap<>();
        OWLOntology owlOntology = OwlService.getInstance().getOwlOntology();
        OWLOntologyManager owlOntologyManager = owlOntology.getOWLOntologyManager();
        OWLDataFactory owlDataFactory = owlOntologyManager.getOWLDataFactory();
//        stringListHashMap.put("PosiadaCertyfikatAdministracyjny|Administacyjne", Arrays.asList("linux_admin_cert", "windows_admin_cert"));
//        stringListHashMap.put("PosiadaCertyfikatyBezpieczenstwa|Bezpieczenstwa", Arrays.asList("advance_security_cert", "basic_security_cert"));
//        stringListHashMap.put("PosiadaCertyfikatyChurowa|Chmurowe", Arrays.asList("aws_cert_cloud", "azure_cert_cloud"));
//        stringListHashMap.put("PosiadaCertyfikatyDevOps|DevOpsCert", Arrays.asList("advance_devops_cert", "basic_devops_cert"));
//        stringListHashMap.put("ZnaJezykProgramowania|JezykProgramowania", Arrays.asList("Cpp_jezyk", "Java_jezyk", "JS_jezyk", "Python_jezyk", "Ruby_jezyk", "Rust_jezyk"));
//        stringListHashMap.put("ZnaBDJezykProgramowania|JezykProgramowania", Arrays.asList("Cpp_jezyk", "Java_jezyk", "JS_jezyk", "Python_jezyk", "Ruby_jezyk", "Rust_jezyk"));
//        stringListHashMap.put("ProwadzilProjekt|Projekt", Arrays.asList("ProjektNaZlecenie_Projekt", "ProjektPubliczy_Projekt", "ProjektPubliczy_Projekt"));
//        stringListHashMap.put("ZnaStosTechnologiczny|StosTechnologiczny", Arrays.asList("Backend_stos", "ML_stos", "FullStack_stos", "Front_stos"));
//        stringListHashMap.put("PosiadaUmiejetnosciMiekkie|UmiejetnosciMiekkie", Arrays.asList("Wspolpraca_miekkie", "UmiejętnościOsobiste_miekkie", "Umiejętności_interpersonalne_miekkie"));
//        stringListHashMap.put("PosiadaUmiejetnosciTwarde|UmiejetnosciTwarde", Arrays.asList("NaprawaKomputera_twarde"));


        stringListHashMap.put("PosiadaCertyfikatAdministracyjny|Administacyjne", new ArrayList<>());
        stringListHashMap.put("PosiadaCertyfikatyBezpieczenstwa|Bezpieczenstwa", new ArrayList<>());
        stringListHashMap.put("PosiadaCertyfikatyChurowa|Chmurowe", new ArrayList<>());
        stringListHashMap.put("PosiadaCertyfikatyDevOps|DevOpsCert", new ArrayList<>());
        stringListHashMap.put("ZnaJezykProgramowania|JezykProgramowania", new ArrayList<>());
        stringListHashMap.put("ZnaBDJezykProgramowania|JezykProgramowania", new ArrayList<>());
        stringListHashMap.put("ProwadzilProjekt|Projekt", new ArrayList<>());
        stringListHashMap.put("ZnaStosTechnologiczny|StosTechnologiczny", new ArrayList<>());
        stringListHashMap.put("PosiadaUmiejetnosciMiekkie|UmiejetnosciMiekkie", new ArrayList<>());
        stringListHashMap.put("PosiadaUmiejetnosciTwarde|UmiejetnosciTwarde", new ArrayList<>());


        for (OWLNamedIndividual namedIndividual : owlOntology.getIndividualsInSignature()) {
            System.out.println(namedIndividual.getIRI().getFragment());
            Set<OWLClassExpression> types = owlDataFactory.getOWLNamedIndividual(namedIndividual.getIRI()).getTypes(owlOntology);
            System.out.println(types.stream().map(owlClassExpression -> ((OWLClassImpl) owlClassExpression).getIRI().getFragment()).collect(Collectors.joining(",")));
            String className = types.stream().map(owlClassExpression -> ((OWLClassImpl) owlClassExpression).getIRI().getFragment()).collect(Collectors.joining(""));
            for (String key : stringListHashMap.keySet()) {
                if (key.contains(className)) {
                    List<String> strings = stringListHashMap.get(key);
                    strings.add(namedIndividual.getIRI().getFragment());
                    stringListHashMap.put(key, strings);
                }
            }
        }


        stanowsika.add("Administrator");
        stanowsika.add("Architekt");
        stanowsika.add("CloudEnginer");
        stanowsika.add("Developer");
        stanowsika.add("SoftwareEnginer");
        stanowsika.add("DevOps");
        stanowsika.add("DevSecOps");
        stanowsika.add("HelpDesk1Linia");
        stanowsika.add("HelpDesk2Linia");
        stanowsika.add("HR");
        stanowsika.add("Pentester");
        stanowsika.add("Programista");
        stanowsika.add("ProjectManager");
    }

    public Pane makeScene(Stage primaryStage) {
        List<Node> nodes = new ArrayList<>();
        OwlService instance = OwlService.getInstance();
        Button button = new Button("Predict");
        nodes.add(button);
        button.autosize();
        button.setLayoutX(1520);
        button.setLayoutY(800);
        AtomicReference<String> predicate = new AtomicReference<>();
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                List<String> predykcje = new ArrayList<>();
                OWLDataFactory factory = instance.getOwlOntology().getOWLOntologyManager().getOWLDataFactory();
                IRI iri = IRI.create("http://www.semanticweb.org/pwitowski/ontologies/2022/0/untitled-ontology-9");

                String kandydatName = getKandydatName();
                System.out.println("Kandydat: " + kandydatName);
                OWLNamedIndividual individual = factory.getOWLNamedIndividual(IRI.create(iri + "#" + kandydatName));
                OWLClass osobaClass = factory.getOWLClass(IRI.create(iri + "#Kandykat"));
                OWLOntologyManager manager = instance.getOwlOntology().getOWLOntologyManager();
                OWLOntology ontology = manager.getOntology(iri);
                OWLClassAssertionAxiom axiom = factory.getOWLClassAssertionAxiom(osobaClass, individual);
                List<AddAxiom> addAxioms = new ArrayList<>();
                AddAxiom addAxiom = new AddAxiom(ontology, axiom);
                addAxioms.add(addAxiom);
                for (String key : stringListHashMap.keySet()) {
                    String name = key.split("\\|")[0];
                    List<String> invididuals = selectedMap.get(key);
                    if (invididuals != null && !invididuals.isEmpty()) {
                        for (String invididualString : invididuals) {
                            OWLNamedIndividual owlNamedIndividual = factory.getOWLNamedIndividual(IRI.create(iri + "#" + invididualString));
                            OWLObjectProperty owlObjectProperty = factory.getOWLObjectProperty(IRI.create(iri + "#" + name));
                            OWLObjectPropertyAssertionAxiom axiom1 = factory
                                    .getOWLObjectPropertyAssertionAxiom(owlObjectProperty, individual, owlNamedIndividual, Collections.emptySet());
                            AddAxiom addAxiom1 = new AddAxiom(ontology, axiom1);
                            addAxioms.add(addAxiom1);
                        }
                    }

                }
                if (!addAxioms.isEmpty()) {
                    manager.applyChanges(addAxioms);
                    try {
                        manager.saveOntology(ontology);
                    } catch (OWLOntologyStorageException ex) {
                        ex.printStackTrace();
                    }
                    OWLReasoner owlReasoner = new Reasoner.ReasonerFactory().createNonBufferingReasoner(ontology);
                    for (String stanowsko : stanowsika) {
                        System.out.println(stanowsko);
                        NodeSet<OWLNamedIndividual> instances = owlReasoner.getInstances(factory.getOWLClass(IRI.create(iri + "#" + stanowsko)), false);
                        for (OWLNamedIndividual instance : instances.getFlattened()) {
                            if (instance.getIRI().toString().contains(kandydatName)) {
                                predykcje.add(stanowsko);
                            }
                        }
                    }

                }

                Label label = new Label("Predykcja: ");
                label.setLayoutX(1720);
                label.setLayoutY(800);
                if (root.getChildren().get(root.getChildren().size() - 1) instanceof Label)
                    root.getChildren().remove(root.getChildren().size() - 1);
                root.getChildren().add(label);
                if (!predykcje.isEmpty()) {
                    String predykcjaLabel = "";
                    for (String predykcja : predykcje) {
                        predykcjaLabel = predykcjaLabel + predykcja + "\n";
                    }
                    Label labelPredykcje = new Label(predykcjaLabel);
                    labelPredykcje.setId("Predykcje");
                    labelPredykcje.setLayoutX(1720);
                    labelPredykcje.setLayoutY(800);
                    if (root.getChildren().get(root.getChildren().size() - 1) instanceof Label)
                        root.getChildren().remove(root.getChildren().size() - 1);
                    root.getChildren().add(labelPredykcje);

                }
            }
        });
        button = new Button("Czyść");
        button.autosize();
        button.setLayoutX(1520);
        button.setLayoutY(840);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                selectedMap = new HashMap<>();
                for (Node child : root.getChildren()) {
                    if (child instanceof ListView) {
                        ListView listView = (ListView) child;
                        listView.getSelectionModel().clearSelection();
                    }
                    if (child instanceof Label) {
                        Label label = (Label) child;
                        if ("Predykcje".equalsIgnoreCase(label.getId())) {
                            label.setText("");
                        }
                    }
                }


            }
        });
        nodes.add(button);

        TextField textField = new TextField();
        textField.setPromptText("Prosze pod imie kandydata");
        textField.setLayoutX(1520);
        textField.setLayoutY(740);
        nodes.add(textField);

        Label label = new Label("Predykcja: ");
        label.setLayoutX(1620);
        label.setLayoutY(800);
        nodes.add(label);


        int i = 0;
        for (String key : stringListHashMap.keySet()) {
            nodes = addNewListView(stringListHashMap.get(key), key.split("\\|")[1], nodes, i, key);
            i++;
        }

        button = new Button("Strona głowna");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.90));
        button.setLayoutY(MainStage.HEIGHT * 0.9);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                primaryStage.getScene().setRoot(new MainScene().makeScene(primaryStage));
            }
        });


        return super.makeScene(nodes);
    }

    private String getKandydatName() {
        String s = UUID.randomUUID().toString();
        for (Node child : root.getChildren()) {
            if (child instanceof TextField) {
                TextField textField = (TextField) child;
                String text = textField.getText();
                if (text != null && !text.isEmpty())
                    s = text;
            }
        }
        return s;
    }

    private List<Node> addNewListView(List<String> items, String names, List<Node> nodes, int i, String key) {
        ListView<String> listView = new ListView<String>();
        ObservableList<String> list = FXCollections.observableArrayList();
        listView.setItems(list);
        listView.autosize();
        list.addAll(items);
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.setOnMouseClicked(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                ObservableList<String> selectedItems = listView.getSelectionModel().getSelectedItems();
                selectedMap.put(key, selectedItems.stream().map(s -> s.toString()).collect(Collectors.toList()));
                System.out.println(selectedMap);
            }
        });
        Label label = new Label(names);
        label.setLayoutX(((0 + 170 * i) + 20) + 50);
        label.setLayoutY(30);
        nodes.add(label);
        listView.setLayoutY(100);
        listView.setLayoutX((0 + 170 * i) + 50);
        nodes.add(listView);
        return nodes;
    }
}
