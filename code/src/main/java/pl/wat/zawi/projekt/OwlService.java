package pl.wat.zawi.projekt;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import java.io.File;

/**
 * @author: Piotr Witowski
 * @date: 09.01.2022
 * @project projekt
 * Day of week: niedziela
 */
public class OwlService {
    private static OwlService owlService = null;
    private OWLOntology owlOntology = null;


    public OwlService() {
    }


    private static OwlService prepareInstanceOfOwlSerice() {
        OwlService owlService = new OwlService();
        owlService.setOwlOntology(owlService.prepareOwlOntology());
        return owlService;
    }

    public static OwlService getInstance() {
        if (owlService == null) {
            owlService = prepareInstanceOfOwlSerice();
        }
        return owlService;
    }

    public OWLOntology prepareOwlOntology()  {
        OWLOntologyManager ontologyManager = OWLManager.createOWLOntologyManager();
        OWLOntology owlOntology = null;
        try {
            File file = new File("C:\\Dyski\\OneDrive\\Studia\\WAT\\II Stopień\\Semestr II\\Zarządzenie wiedzą\\Projekt\\rekrutacjaNowaV7.owl");
            owlOntology = ontologyManager.loadOntologyFromOntologyDocument(file);
            return owlOntology;
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return null;
    }


    public OWLOntology getOwlOntology() {
        return owlOntology;
    }

    public void setOwlOntology(OWLOntology owlOntology) {
        this.owlOntology = owlOntology;
    }
}
