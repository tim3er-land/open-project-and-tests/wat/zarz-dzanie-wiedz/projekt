package pl.wat.zawi.projekt;

import javafx.application.Application;
import javafx.stage.Stage;
import pl.wat.zawi.projekt.stage.MainStage;

import static javafx.application.Application.launch;

/**
 * @author: Piotr Witowski
 * @date: 26.12.2021
 * @project projekt
 * Day of week: niedziela
 */
public class ProjectMain  {
    public static void main(String[] args) {
        launch(MainStage.class, args);
    }
}
