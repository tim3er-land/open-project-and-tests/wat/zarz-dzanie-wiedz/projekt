package pl.wat.zawi.projekt.scene;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import pl.wat.zawi.projekt.OwlService;
import pl.wat.zawi.projekt.stage.MainStage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: Piotr Witowski
 * @date: 09.01.2022
 * @project projekt
 * Day of week: niedziela
 */
public class InfoScene extends CustomScene {
    public Pane makeScene(Stage primaryStage) {
        IRI iri = IRI.create("http://www.semanticweb.org/pwitowski/ontologies/2022/0/untitled-ontology-9");
        List<Node> nodes = new ArrayList<>();
        OwlService instance = OwlService.getInstance();
        Button button = new Button("Strona głowna");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.90));
        button.setLayoutY(MainStage.HEIGHT * 0.9);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                primaryStage.getScene().setRoot(new MainScene().makeScene(primaryStage));
            }
        });
        String classes = instance.getOwlOntology().getClassesInSignature().stream().map(owlClass -> owlClass.getIRI().getFragment()).collect(Collectors.joining("\n"));
        Label label = new Label(classes);
        label.setLayoutX((MainStage.WIDTH * 0.01));
        label.setLayoutY(MainStage.HEIGHT * 0.06);
        nodes.add(label);
        label = new Label("Klasy: ");
        label.setLayoutX((MainStage.WIDTH * 0.01));
        label.setLayoutY(MainStage.HEIGHT * 0.01);
        label.setFont(new Font(30));
        nodes.add(label);

        String individuals = instance.getOwlOntology().getIndividualsInSignature().stream().map(owlClass -> owlClass.getIRI().getFragment()).collect(Collectors.joining("\n"));
        label = new Label(individuals);
        label.setLayoutX((MainStage.WIDTH * 0.2));
        label.setLayoutY(MainStage.HEIGHT * 0.06);
        nodes.add(label);
        label = new Label("Individuals: ");
        label.setLayoutX((MainStage.WIDTH * 0.2));
        label.setLayoutY(MainStage.HEIGHT * 0.01);
        label.setFont(new Font(30));
        nodes.add(label);

        String objectProperties = instance.getOwlOntology().getObjectPropertiesInSignature().stream().map(owlClass -> owlClass.getIRI().getFragment()).collect(Collectors.joining("\n"));
        label = new Label(objectProperties);
        label.setLayoutX((MainStage.WIDTH * 0.4));
        label.setLayoutY(MainStage.HEIGHT * 0.06);
        nodes.add(label);
        label = new Label("object properties: ");
        label.setLayoutX((MainStage.WIDTH * 0.4));
        label.setLayoutY(MainStage.HEIGHT * 0.01);
        label.setFont(new Font(30));
        nodes.add(label);


        return super.makeScene(nodes);
    }

    private String getAllOwlClass(OwlService instance) {
        List<String> names = new ArrayList<>();
        for (OWLClass owlClass : instance.getOwlOntology().getClassesInSignature()) {
            names.add(owlClass.getIRI().getNamespace());
        }
        return names.stream().collect(Collectors.joining("\n"));
    }
}
