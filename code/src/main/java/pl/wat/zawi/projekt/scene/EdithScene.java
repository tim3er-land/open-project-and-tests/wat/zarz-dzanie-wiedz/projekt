package pl.wat.zawi.projekt.scene;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.OWLEntityRemover;
import org.semanticweb.owlapi.util.OWLEntityRenamer;
import pl.wat.zawi.projekt.OwlService;
import pl.wat.zawi.projekt.stage.MainStage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: Piotr Witowski
 * @date: 09.01.2022
 * @project projekt
 * Day of week: niedziela
 */
public class EdithScene extends CustomScene {

    private static List<String> individualClasses = new ArrayList<>();
    private String selectedValue = "";
    private String selectedValueDelete = "";
    private String selectedValueEdith = "";

    public EdithScene() {
        individualClasses.add("Administacyjne");
        individualClasses.add("Bezpieczenstwa");
        individualClasses.add("Chmurowe");
        individualClasses.add("DevOpsCert");
        individualClasses.add("JezykProgramowania");
        individualClasses.add("Projekt");
        individualClasses.add("StosTechnologiczny");
        individualClasses.add("UmiejetnosciMiekkie");
        individualClasses.add("UmiejetnosciTwarde");
    }

    public Pane makeScene(Stage primaryStage) {
        IRI iri = IRI.create("http://www.semanticweb.org/pwitowski/ontologies/2022/0/untitled-ontology-9");
        List<Node> nodes = new ArrayList<>();
        OwlService instance = OwlService.getInstance();
        OWLOntologyManager owlOntologyManager = instance.getOwlOntology().getOWLOntologyManager();
        OWLDataFactory owlDataFactory = owlOntologyManager.getOWLDataFactory();
        Button button = new Button("Strona głowna");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.90));
        button.setLayoutY(MainStage.HEIGHT * 0.9);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                primaryStage.getScene().setRoot(new MainScene().makeScene(primaryStage));
            }
        });

        addNodeForAddingNewIndividualns(iri, nodes, owlOntologyManager, owlDataFactory,primaryStage);
        addNodeForAddingEdithIndividualns(iri, nodes, owlOntologyManager, owlDataFactory,primaryStage);
        addNodeForAddingDeleteIndividualns(iri, nodes, owlOntologyManager, owlDataFactory, primaryStage);
        return super.makeScene(nodes);
    }

    private void addNodeForAddingDeleteIndividualns(IRI iri, List<Node> nodes, OWLOntologyManager owlOntologyManager, OWLDataFactory owlDataFactory, Stage primaryStage) {
        List<String> individuals = OwlService.getInstance().getOwlOntology().getIndividualsInSignature().stream().map(owlClass -> owlClass.getIRI().getFragment()).collect(Collectors.toList());
        Button button;
        Label label = new Label("Usuniecie");
        label.setLayoutX(MainStage.WIDTH * 0.8);
        label.setLayoutY(MainStage.HEIGHT * 0.1);
        label.setFont(new Font(40));
        nodes.add(label);

        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(individuals));
        cb.setLayoutX((MainStage.WIDTH * 0.8));
        cb.setLayoutY(MainStage.HEIGHT * 0.2);
        cb.setOnAction(event -> {
            selectedValueDelete = cb.getValue().toString();
            System.out.println(cb.getValue());
        });
        nodes.add(cb);

        button = new Button("Usuń indiwidułum");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.8));
        button.setLayoutY(MainStage.HEIGHT * 0.35);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (selectedValueDelete.isEmpty())
                    return;
                OWLOntology ontology = owlOntologyManager.getOntology(iri);
                OWLNamedIndividual individual = owlDataFactory.getOWLNamedIndividual(IRI.create(iri + "#" + selectedValueDelete));
                OWLEntityRemover remover = new OWLEntityRemover(owlOntologyManager, Collections.singleton(ontology));
                remover.visit(individual);
                individual.accept(remover);
                owlOntologyManager.applyChanges(remover.getChanges());
                try {
                    owlOntologyManager.saveOntology(ontology);
                    primaryStage.getScene().setRoot(new MainScene().makeScene(primaryStage));
                } catch (OWLOntologyStorageException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void addNodeForAddingEdithIndividualns(IRI iri, List<Node> nodes, OWLOntologyManager owlOntologyManager, OWLDataFactory owlDataFactory, Stage primaryStage) {
        List<String> individuals = OwlService.getInstance().getOwlOntology().getIndividualsInSignature().stream().map(owlClass -> owlClass.getIRI().getFragment()).collect(Collectors.toList());
        Button button;
        Label label = new Label("Edycja");
        label.setLayoutX(MainStage.WIDTH * 0.5);
        label.setLayoutY(MainStage.HEIGHT * 0.1);
        label.setFont(new Font(40));
        nodes.add(label);

        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(individuals));
        cb.setLayoutX((MainStage.WIDTH * 0.5));
        cb.setLayoutY(MainStage.HEIGHT * 0.2);
        cb.setOnAction(event -> {
            selectedValueEdith = cb.getValue().toString();
            System.out.println(cb.getValue());
        });
        nodes.add(cb);


        TextField textField = new TextField();
        textField.setPromptText("Podaj nową nazwe indywidułum");
        textField.setLayoutX(MainStage.WIDTH * 0.5);
        textField.setLayoutY(MainStage.HEIGHT * 0.3);
        nodes.add(textField);

        button = new Button("Edytuj indiwidułum");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.5));
        button.setLayoutY(MainStage.HEIGHT * 0.35);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (selectedValueEdith.isEmpty() ||textField.getText() == null || textField.getText().isEmpty())
                    return;
                OWLOntology ontology = owlOntologyManager.getOntology(iri);
                OWLNamedIndividual individual = owlDataFactory.getOWLNamedIndividual(IRI.create(iri + "#" + selectedValueEdith));
                final OWLEntityRenamer renamer = new OWLEntityRenamer(owlOntologyManager, Collections.singleton(ontology));
                final Map<OWLEntity, IRI> entity2IRIMap = new HashMap<>();
                ontology.getIndividualsInSignature().stream().filter(owlNamedIndividual -> owlNamedIndividual.getIRI().equals(individual.getIRI())).forEach(owlNamedIndividual -> {
                    String name = owlNamedIndividual.getIRI().getFragment();
                    entity2IRIMap.put(owlNamedIndividual, IRI.create(owlNamedIndividual.getIRI().toString().replace(name, textField.getText())));

                });
                owlOntologyManager.applyChanges(renamer.changeIRI(entity2IRIMap));
                try {
                    owlOntologyManager.saveOntology(ontology);
                    primaryStage.getScene().setRoot(new MainScene().makeScene(primaryStage));
                } catch (OWLOntologyStorageException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void addNodeForAddingNewIndividualns(IRI iri, List<Node> nodes, OWLOntologyManager owlOntologyManager, OWLDataFactory owlDataFactory, Stage primaryStage) {
        Button button;
        Label label = new Label("Dodanie");
        label.setLayoutX(MainStage.WIDTH * 0.1);
        label.setLayoutY(MainStage.HEIGHT * 0.1);
        label.setFont(new Font(40));
        nodes.add(label);

        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(individualClasses));
        cb.setLayoutX((MainStage.WIDTH * 0.1));
        cb.setLayoutY(MainStage.HEIGHT * 0.2);
        cb.setOnAction(event -> {
            selectedValue = cb.getValue().toString();
            System.out.println(cb.getValue());
        });
        nodes.add(cb);


        TextField textField = new TextField();
        textField.setPromptText("Prosze pod nazwe indywidułum");
        textField.setLayoutX(MainStage.WIDTH * 0.1);
        textField.setLayoutY(MainStage.HEIGHT * 0.3);
        nodes.add(textField);

        button = new Button("Dodaj indiwidułum");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.1));
        button.setLayoutY(MainStage.HEIGHT * 0.35);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                boolean ftBool = !selectedValue.isEmpty();
                String newIndividuumName = "";
                for (Node child : root.getChildren()) {
                    if (child instanceof TextField) {
                        TextField tf = (TextField) child;
                        newIndividuumName = tf.getText();
                    }
                }
                if (ftBool && !newIndividuumName.isEmpty()) {
                    OWLClass osobaClass = owlDataFactory.getOWLClass(IRI.create(iri + "#" + selectedValue));
                    OWLNamedIndividual individual = owlDataFactory.getOWLNamedIndividual(IRI.create(iri + "#" + newIndividuumName));
                    OWLClassAssertionAxiom axiom = owlDataFactory.getOWLClassAssertionAxiom(osobaClass, individual);
                    OWLOntology ontology = owlOntologyManager.getOntology(iri);
                    AddAxiom addAxiom = new AddAxiom(ontology, axiom);
                    owlOntologyManager.applyChange(addAxiom);
                    try {
                        owlOntologyManager.saveOntology(ontology);
                        primaryStage.getScene().setRoot(new MainScene().makeScene(primaryStage));
                    } catch (OWLOntologyStorageException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }
}
