package pl.wat.zawi.projekt.scene;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import pl.wat.zawi.projekt.stage.MainStage;

import java.util.ArrayList;
import java.util.List;


/**
 * @author: Piotr Witowski
 * @date: 09.01.2022
 * @project projekt
 * Day of week: niedziela
 */
public class MainScene extends CustomScene {
    public Pane makeScene(Stage primaryStage) {
        List<Node> nodes = new ArrayList<>();

        Button button = new Button("Owl Info");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.50));
        button.setLayoutY(MainStage.HEIGHT * 0.5);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                primaryStage.getScene().setRoot(new InfoScene().makeScene(primaryStage));
            }
        });
        button = new Button("Predict Owl");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.40));
        button.setLayoutY(MainStage.HEIGHT * 0.5);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                primaryStage.getScene().setRoot(new PredictScene().makeScene(primaryStage));
            }
        });
        button = new Button("Add/Edit/Delete individiuals");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.6));
        button.setLayoutY(MainStage.HEIGHT * 0.5);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                primaryStage.getScene().setRoot(new EdithScene().makeScene(primaryStage));
            }
        });
        Label label = new Label("Projekt z przedmiotu");
        label.setLayoutX(MainStage.WIDTH * 0.40);
        label.setLayoutY(MainStage.HEIGHT * 0.2);
        label.setFont(new Font(40));
        nodes.add(label);

        label = new Label("Zarządzanie wiedzą");
        label.setLayoutX(MainStage.WIDTH * 0.40);
        label.setLayoutY(MainStage.HEIGHT * 0.25);
        label.setFont(new Font(30));
        nodes.add(label);

        label = new Label("Rekrutacja");
        label.setLayoutX(MainStage.WIDTH * 0.40);
        label.setLayoutY(MainStage.HEIGHT * 0.30);
        label.setFont(new Font(30));
        nodes.add(label);

        label = new Label("Projekt wykonali: ");
        label.setLayoutX(MainStage.WIDTH * 0.40);
        label.setLayoutY(MainStage.HEIGHT * 0.35);
        label.setFont(new Font(20));
        nodes.add(label);

        label = new Label("Piotr Witowski");
        label.setLayoutX(MainStage.WIDTH * 0.40);
        label.setLayoutY(MainStage.HEIGHT * 0.38);
        label.setFont(new Font(15));
        nodes.add(label);

        label = new Label("Michał Karbarz");
        label.setLayoutX(MainStage.WIDTH * 0.40);
        label.setLayoutY(MainStage.HEIGHT * 0.40);
        label.setFont(new Font(15));
        nodes.add(label);

        button = new Button("Zamknij");
        nodes.add(button);
        button.setLayoutX((MainStage.WIDTH * 0.5));
        button.setLayoutY(MainStage.HEIGHT * 0.6);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Platform.exit();
                System.exit(0);
            }
        });

        return super.makeScene(nodes);
    }
}
