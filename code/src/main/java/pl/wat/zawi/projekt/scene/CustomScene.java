package pl.wat.zawi.projekt.scene;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

import java.util.List;

/**
 * @author: Piotr Witowski
 * @date: 09.01.2022
 * @project projekt
 * Day of week: niedziela
 */
public abstract class CustomScene {
    protected Pane root;

    protected CustomScene() {
        root = new Pane();
    }

    public Scene makeScene() {
        return null;
    }

    protected Pane makeScene(List<Node> nodes) {
        return addElementToPane(nodes);
    }

    public Pane addElementToPane(List<Node> nodes) {
        root = new Pane();
        for (Node node : nodes) {
            root.getChildren().add(node);
        }
        return root;
    }

    protected Scene prepareScene(Pane root, int x, int y) {
        return new Scene(root, x, y);
    }
}
